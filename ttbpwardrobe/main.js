var app = new Vue({
    el: "#app",
    data: {
        skinTokens: 110,
        gems: 350,
        selectedSkin: 0,
        inViewSkin: 0,
        //home, wardrobe, reward
        scene: 'home',
        reward: '',
        skins: {
            0: {
                character: 'tom',
                status: 1,
                priceTickets: 10,
                priceGems: 100,
            },
            1: {
                character: 'tom',
                status: 1,
                priceTickets: 20,
                priceGems: 200,
            },
            2: {
                character: 'tom',
                status: 0,
                priceTickets: 40,
                priceGems: 400,
            },
            3: {
                character: 'tom',
                status: 0,
                priceTickets: 100,
                priceGems: 1000,
            },
            4: {
                character: 'angela',
                status: 1,
                priceTickets: 10,
                priceGems: 100,
            },
            5: {
                character: 'angela',
                status: 0,
                priceTickets: 20,
                priceGems: 200,
            },
            6: {
                character: 'angela',
                status: 0,
                priceTickets: 40,
                priceGems: 400,
            },
            7: {
                character: 'angela',
                status: 0,
                priceTickets: 100,
                priceGems: 1000,
            },
            8: {
                character: 'ginger',
                status: 1,
                priceTickets: 20,
                priceGems: 200,
            },
            9: {
                character: 'ginger',
                status: 0,
                priceTickets: 40,
                priceGems: 400,
            },
            10: {
                character: 'ginger',
                status: 0,
                priceTickets: 100,
                priceGems: 1000,
            },
            11: {
                character: 'becca',
                status: 1,
                priceTickets: 20,
                priceGems: 200,
            },
            12: {
                character: 'becca',
                status: 0,
                priceTickets: 40,
                priceGems: 400,
            },
            13: {
                character: 'becca',
                status: 0,
                priceTickets: 100,
                priceGems: 1000,
            },
            14: {
                character: 'hank',
                status: -1,
                priceTickets: 20,
                priceGems: 200,
            },
            15: {
                character: 'hank',
                status: -1,
                priceTickets: 40,
                priceGems: 400,
            },
            16: {
                character: 'hank',
                status: -1,
                priceTickets: 100,
                priceGems: 1000,
            },
            17: {
                character: 'ben',
                status: -1,
                priceTickets: 20,
                priceGems: 400,
            },
            18: {
                character: 'ben',
                status: -1,
                priceTickets: 40,
                priceGems: 400,
            },
            19: {
                character: 'ben',
                status: -1,
                priceTickets: 100,
                priceGems: 1000,
            },
        },
        worlds: {
            0: {
                unlocked: true,
            },
            1: {
                unlocked: false,
            },
            2: {
                unlocked: false,
            },
        },
        currentWorld: 0,
        
        //style help
        popup: false,
    },
    methods: {
        //change scene
        mChangeScene: function(newScene) {
            this.scene = newScene;
        },
        //change scene to wardrobe
        mChangeSceneToWardrobe: function() {
            this.inViewSkin = this.selectedSkin;
            this.mChangeScene('wardrobe');
        },
        //closed wardrobe without changing outfit
        mWardrobeCancel: function() {
            this.inViewSkin = this.selectedSkin;
            
            this.mChangeScene("home");
        },
        //get URL for a skin by the skin ID
        mGetSkinURL: function(skinId) {
            return "images/" + skinId + ".png";
        },
        //get skin price by the skin ID
        mGetSkinPrice: function(skinID) {
            return this.skins[skinID].status == 0 ? this.skins[skinID].priceTickets : this.skins[skinID].priceGems;
        },
        //get skin status by the skin ID
        mGetSkinStatus: function(skinID) {
            return this.skins[skinID].status;
        },
        //when player taps on another skin in grid
        mSetChangeSkinInView: function(skinID) {
            this.inViewSkin = skinID;
        },
        //purchasing a skin
        mSkinPurchaseInitiation: function() {
            //set reward type
            this.reward = 'skin';
            
            //change scene to reward
            this.mChangeScene("reward");
            
            //spending tickets
            if(this.mGetSkinStatus(this.inViewSkin) == 0) {
                this.skinTokens -= this.mGetSkinPrice(this.inViewSkin);
            }
            //spending gems
            else {
                this.gems -= this.mGetSkinPrice(this.inViewSkin);
            }
            
            this.skins[this.inViewSkin].status = 1;
            
            this.selectedSkin = this.inViewSkin;
        },
        //change character/skin used for gameplay
        mChangeSelectedSkin: function() {
            this.selectedSkin = this.inViewSkin;
            
            this.mChangeScene("home");
        },
        mShowPopup: function(pType) {
            this.popup = pType;
        },
        mHidePopup: function() {
            this.popup = false;
        },
        mGetCharacter: function(skinID) {
            return this.skins[skinID].character;
        },
        mVideoPosition: function() {
            this.scene = "ad";
            
            
            setTimeout(function () { 
                this.reward = "skin-tickets";
                
                this.mChangeScene("reward");
            }.bind(this), 2000);
        },
        mClaimSkinTickets: function() {
            this.skinTokens += 5;
            this.scene = "wardrobe";
        },
        mNextWorld: function() {
            this.currentWorld += 1;
            console.log(this.currentWorld);
        },
        mPreviousWorld: function(world) {
            this.currentWorld -= 1;
            console.log(this.currentWorld);
        },
        mActivePopup: function(world) {
            return this.currentWorld == world;
        },
        mChangeToLockedWorld: function() {
            this.currentWorld = 1;
            this.mChangeScene("home");
        },
    },
    computed: {
        //get image url for currently selected character
        cSelectedSkinURL: function() {
            return this.mGetSkinURL(this.selectedSkin);
        },
        cInViewSkinURL: function() {
            return this.mGetSkinURL(this.inViewSkin);
        },
        //returns true if player has enouch currency to buy skin
        cSkinEnoughCurrency: function() {
            //spending tickets
            if(this.mGetSkinStatus(this.inViewSkin) == 0) {
                return this.skinTokens >= this.mGetSkinPrice(this.inViewSkin);
            }
            //spending gems
            else {
                return this.gems >= this.mGetSkinPrice(this.inViewSkin);
            }
        },
        cCharacterInView: function() {
            return this.mGetCharacter(this.inViewSkin);
        },
        cLeftArrow: function() {
            return this.currentWorld > 0;
        },
        cRightArrow: function() {
            return this.currentWorld < 2;
        },
        cHubOffset: function() {
            return "vw" + this.currentWorld * 100;
        },
    },
});

/*
var app = new Vue({
    el: "#app",
    data: {
        tools: 1000,
        merryGoAround: {
            level: 0,
            maxLevel: 2,
            investedTools: 0,
            hideUI: false,
            progression: {
                0: {
                    image: 'images/building-1-level-0.png',
                    stepPrice: 20,
                    steps: 4,
                    step: 0,
                },
                1: {
                    image: 'images/building-1-level-1.png',
                    stepPrice: 30,
                    steps:6,
                    step: 0,
                },
                2: {
                    image: 'images/building-1-level-2.png',
                }
            }
        }
    },
    methods: {
        test: function() {},
        
        progressBarWidth: function(maxSteps, currentStep) {
            console.log(maxSteps);
            return 96*(currentStep/maxSteps);
        },
        buy: function(attraction) {
            console.log("buy")
            level = this.merryGoAround.level;
            
            //don't allow spending tools for nothing
            if(this.merryGoAround.progression[level].step >= this.merryGoAround.progression[level].steps)
                return false;
            
            //increase step in progres bar
            this.merryGoAround.progression[level].step += 1;
            
            //spend tools
            this.tools -= this.merryGoAround.progression[level].stepPrice;
            
            console.log(this.merryGoAround.progression[level].step)
            console.log(this.merryGoAround.progression[level].steps)
            
            //progres bar full
            if(this.merryGoAround.progression[level].step >= this.merryGoAround.progression[level].steps) {
                this.levelUpInitiate();
                console.log("go level up!")
            } 
        },
        levelUpInitiate: function(attraction) {
            console.log("level up");
            this.hideUI = true;
            
            this.merryGoAround.level += 1;
            
        } 
    },
    computed: {
    },
    mounted(){
        this.test();
    },
});*/