var app = new Vue({
    el: "#app",
    data: {
        tools: 2000,
        hideUI: true,
        tutorial: false,
        merryGoAround: {
            level: 0,
            maxLevel: 2,
            upgrade: 0,
            lvlup: false,
            attention: 0,
            progression: {
                0: {
                    image: 'images/building-1-level-0.png',
                    stepPrice: 20,
                    steps: 4,
                    step: 0,
                },
                1: {
                    image: 'images/building-1-level-1.png',
                    stepPrice: 30,
                    steps:8,
                    step: 0,
                },
                2: {
                    image: 'images/building-1-level-2.png',
                }
            }
        },
        ferrisWheel: {
            level: 0,
            maxLevel: 2,
            upgrade: 0,
            lvlup: false,
            attention: 0,
            progression: {
                0: {
                    image: 'images/building-2-level-0.png',
                    stepPrice: 20,
                    steps: 4,
                    step: 0,
                },
                1: {
                    image: 'images/building-2-level-1.png',
                    stepPrice: 30,
                    steps:8,
                    step: 0,
                },
                2: {
                    image: 'images/building-2-level-2.png',
                }
            }
        },
        rollerCoaster: {
            level: 0,
            maxLevel: 4,
            upgrade: 0,
            lvlup: false,
            attention: 0,
            progression: {
                0: {
                    image: 'images/building-3-level-0.png',
                    stepPrice: 20,
                    steps: 2,
                    step: 0,
                },
                1: {
                    image: 'images/building-3-level-1.png',
                    stepPrice: 30,
                    steps:4,
                    step: 0,
                },
                2: {
                    image: 'images/building-3-level-2.png',
                    stepPrice: 30,
                    steps:8,
                    step: 0,
                },
                3: {
                    image: 'images/building-3-level-3.png',
                    stepPrice: 30,
                    steps:8,
                    step: 0,
                },
                4: {
                    image: 'images/building-3-level-4.png',
                }
            }
        },
    },
    methods: {
        initiate: function() {
            setTimeout(function () {
                this.tutorial = true;
            }.bind(this), 1000);
            setTimeout(function () {
                this.hideUI = false;
            }.bind(this), 1300);
        },
        
        progressBarWidth: function(maxSteps, currentStep) {
            return 88*(currentStep/maxSteps);
        },
        buy: function(gAttraction) {
            var attraction = this.getAttraction(gAttraction);
            
            level = attraction.level;
            
            //don't allow spending tools for nothing
            if(attraction.progression[level].step >= attraction.progression[level].steps)
                return false;
            
            attraction.upgrade += 1;
            
            //increase step in progres bar
            attraction.progression[level].step += 1;
            
            //spend tools
            this.tools -= attraction.progression[level].stepPrice;
            
            //progres bar full
            if(attraction.progression[level].step >= attraction.progression[level].steps) {
                this.levelUp(attraction);
            }
            
            setTimeout(function () {
                attraction.upgrade -= 1;
            }.bind(this), 500);
        },
        levelUp: function(attraction) {
            this.tutorial = false;
            this.hideUI = true;
            attraction.upgrade = 0;
            attraction.lvlup = true;
            
            
            setTimeout(function () {
                attraction.level += 1;
            }.bind(this), 1000);
            
            setTimeout(function () {
                this.hideUI = false;
                attraction.lvlup = false;
                attraction.upgrade = 0;
            }.bind(this), 2000);
        },
        getAttraction(gAttraction) {
            return (gAttraction == "merryGoAround") ? this.merryGoAround : (gAttraction == "ferrisWheel") ? this.ferrisWheel : this.rollerCoaster;
        },
        wobbleUI: function(gAttraction) {
            if(this.hideUI)
                return false;
            
            var attraction = this.getAttraction(gAttraction);
            console.log("is been click")
            
            attraction.attention += 1;
            setTimeout(function () {
                attraction.attention -= 1;
            }.bind(this), 500);
        }
    },
    computed: {
        isEndOfWorld: function(){
            if(this.merryGoAround.level >= this.merryGoAround.maxLevel && this.ferrisWheel.level >= this.ferrisWheel.maxLevel && this.rollerCoaster.level >= this.rollerCoaster.maxLevel)
                return true;
            else
                return false;
        },
    },
    mounted(){
        this.initiate();
    },
});

/*
var appaaa = new ({
    el: "#appaaa",

    data: {
        ytApiReady: false,
        goldBarsRequirement: 40,
        resetGoldBar: false,
        nextKey: 90,
        level: 1,
        upgrading: false,
        gameOvering: false,
        caliming: false,
        activeScene: {
            gameOver: false,
            home: true,
            lootBoxes: false,
            lootBoxOpening: false,
            running: false,
            timeMachine: false,
            watchAD: false,
        },
        lootBoxOpening: 1,
        rewards: {
        },
        runnerVideos: {
            1: {
                active: false,
                goldbars: 54,
                minLevel: 1,
                score: 550,
                used: false,
                url: '9kQBPM-KVrM',
            },
            2: {
                active: false,
                goldbars: 41,
                minLevel: 1,
                score: 2040,
                used: false,
                url: 'YtlIuut-E5c',
            },
            3: {
                active: false,
                goldbars: 56,
                minLevel: 1,
                score: 1130,
                used: false,
                url: 'u-J79ciyRRg',
            },
            4: {
                active: false,
                goldbars: 113,
                minLevel: 2,
                score: 1130,
                used: false,
                url: 'DVUL_YcWoWY',
            },
            5: {
                active: false,
                goldbars: 372,
                minLevel: 2,
                score: 1130,
                used: false,
                url: 'Kq0Yw0bpI0I',
            },
            6: {
                active: false,
                goldbars: 67,
                minLevel: 4,
                score: 1130,
                used: false,
                url: 'V3w7LRMCJVg',
            },
            7: {
                active: false,
                goldbars: 134,
                minLevel: 4,
                score: 1130,
                used: false,
                url: 'v9Angx_fxvY',
            },
            specialPathUnlock: {
                active: false,
                goldbars: 134,
                minLevel: 100,
                score: 1130,
                used: false,
                url: 'qlbhZHjNj94',
            },
            eraUnlock: {
                active: false,
                goldbars: 67,
                minLevel: 100,
                score: 1130,
                used: false,
                url: 'V3w7LRMCJVg',
            },
        },

        progression: {
            1: {
                resources: {
                    green: 30,
                },
                reward: {
                    type: "special-path",
                    image: "images/reward-special-path.png",
                }
            },
            2: {
                resources: {
                    green: 50,
                    pink: 50,
                },
                reward: {
                    type: "loot-box",
                    image: "images/loot-box-chest.png",
                }
            },
            3: {
                resources: {
                    green: 60,
                    pink: 60,
                    orange: 30,
                },
                reward: {
                    type: "era",
                    image: "images/reward-new-era.png",
                }
            },
            4: {
                resources: {
                    green: 50,
                    pink: 50,
                    orange: 50,
                    purple: 50,
                    blue: 50,
                    yellow: 50,
                },
                reward: {
                    type: "loot-box",
                    image: "images/loot-box-chest.png",
                }
            },
            5: {
                resources: {
                    green: 200,
                    pink: 200,
                    orange: 200,
                    purple: 200,
                    blue: 200,
                    yellow: 200,
                },
                reward: {
                    type: "loot-box",
                    image: "images/loot-box-chest.png",
                }
            }
        },
        inventory: {
            hardCurrency: 100,
            chests: 0,
            goldBars: 0,
            keys: 1,
            resources: {
                green: {
                    active: true,
                    amount: 0,
                    color: "#90C88E",
                    fill: 0,
                    preFill: 0,
                },
                pink: {
                    active: false,
                    amount: 0,
                    color: "#C88EA5",
                    fill: 0,
                    preFill: 0,
                },
                orange: {
                    active: false,
                    amount: 0,
                    color: "#C8A38D",
                    fill: 0,
                    preFill: 0,
                },
                purple: {
                    active: false,
                    amount: 0,
                    color: "#958DC8",
                    fill: 0,
                    preFill: 0,
                },
                blue: {
                    active: false,
                    amount: 0,
                    color: "#8EBFC8",
                    fill: 0,
                    preFill: 0,
                },
                yellow: {
                    active: false,
                    amount: 0,
                    color: "#C8BD8E",
                    fill: 0,
                    preFill: 0,
                },
            }
        },
        resources: ["green", "pink", "orange", "purple", "blue", "yellow"]
    },

    methods: {
        //Transition from one scene to another
        changeScene: function(newScene) {
            this.upgrading = false;
            
            //Hide all scenes
            for (scene in this.activeScene) {
                this.activeScene[scene] = false;
            }

            //Show new scene
            this.activeScene[newScene] = true;

            //Reset loot box opening
            this.lootBoxOpening = 1;

            if(newScene == "timeMachine") {
                setTimeout(function () { this.stmLightsBackground(); }.bind(this), 10);
            }
            if(newScene == "gameOver") {
                setTimeout(function () { this.gameOver(); }.bind(this), 100);
            }
        },

        //Watch AD
        watchAD: function(nextScene) {
            this.changeScene("watchAD");
            this.inventory.keys += 1;

            setTimeout(function () { this.changeScene("lootBoxOpening") }.bind(this), 2000);
        },

        //Open loot box sequence
        openLootBox: function() {
            this.lootBoxOpeningNumber(2);
            this.generateRewards("chest");
            this.inventory.chests -= 1;
            this.inventory.keys -= 1;
        },

        //Generate rewards
        generateRewards: function(type) {
            this.destroyRewards();

            if(type == "chest") {
                rnmb = Math.floor(Object.keys(this.progression[this.level].resources).length / 3);
                mnmb = (Object.keys(this.progression[this.level].resources).length < 2) ? 1 : 2;
                
                numberOfResources = mnmb + Math.floor(Math.random() * rnmb);

                i = 0;
                while(i < numberOfResources){
                    var r = Math.floor(Math.random() * this.resources.length);

                    if(!this.progression[this.level].resources[this.resources[r]]) continue;
                    if(this.resources[r] in this.rewards.resources) continue;

                    Vue.set(this.rewards.resources, this.resources[r], {});
                    Vue.set(this.rewards.resources[this.resources[r]], "amount", Math.floor(Math.random() * 20) + 30);
                    Vue.set(this.rewards.resources[this.resources[r]], "position", {});
                    Vue.set(this.rewards.resources[this.resources[r]].position, "left", 0);
                    Vue.set(this.rewards.resources[this.resources[r]].position, "top", 0);
                    i++;
                }

                setTimeout(function () { this.lootBoxOpeningNumber(3) }.bind(this), 1200);
                setTimeout(function () { this.updateResourceAmount() }.bind(this), 2500);
            }
        },

        //Calculates existing resource + reward for all resources
        updateResourceAmount: function() {
            for (resource in this.rewards.resources) {
                this.inventory.resources[resource].amount = this.inventory.resources[resource].amount + this.rewards.resources[resource].amount;
            }
            this.preFillRewards();
        },

        //Update prefill
        preFillRewards: function() {
            //loop through all the resources
            for (resource in this.inventory.resources) {
                //skip if the resource is not used
                if(!this.inventory.resources[resource].active)
                    continue;

                resourceRequirement = this.progression[this.level].resources[resource];

                amount = this.inventory.resources[resource].amount;

                this.inventory.resources[resource].preFill = (amount > resourceRequirement) ? 100 : (amount / resourceRequirement * 100);
            }
        },

        //Updates existing resources to be the same as prefill
        fillRewards: function() {
            for(resource in this.inventory.resources) {
                this.inventory.resources[resource].fill = this.inventory.resources[resource].preFill;
            }
        },

        destroyRewards: function() {
            this.rewards = {
                resources: {}
            };
        },

        //Loot box sequence start
        lootBoxOpeningNumber: function(number) {
            this.lootBoxOpening = number;
        },

        //Get image for resource
        resourceImage: function(name) {
            return "images/resource-" + name + ".png";
        },

        //Get image for single resource
        resourceSingleImage: function(name) {
            return "images/resource-single-" + name + ".png";
        },

        claimResources: function() {
            setTimeout(function () { 
                //Get resource position relative to screen
                for (resource in this.rewards.resources) {
                    this.rewards.resources[resource].position = {
                        left: this.getOffset( document.getElementById('lbo_' + resource) ).left,
                        top: this.getOffset( document.getElementById('lbo_' + resource) ).top,
                    }
                }

                //calculate where do the resources have to move
                setTimeout(function () { this.resourceClaimFinalDestination(); }.bind(this), 500);
                setTimeout(function () { this.fillRewards(); }.bind(this), 100);

                this.lootBoxOpening = 4;
                this.hideElement("lbo-resources", 0.5, 2);
                this.hideElement("lbo", 0.5, 3.5);
                setTimeout(function () { this.changeScene("lootBoxes"); }.bind(this), 4000);
                setTimeout(function () { this.destroyRewards(); }.bind(this), 5000);
            }.bind(this), 700);
            
        },

        //get absolute position of element relative to window
        getOffset: function( el ) {
            var _x = 0;
            var _y = 0;
            while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
                _x += el.offsetLeft - el.scrollLeft;
                _y += el.offsetTop - el.scrollTop;
                el = el.offsetParent;
            }
            return { top: _y, left: _x };
        },
        //calculate for resources where do they have to move
        resourceClaimFinalDestination: function() {
            //add class for apeparance/disappearance animation
            for (resource in this.rewards.resources) {
                document.getElementById("lar_"+resource+"_1").classList.add('app-dis');
                document.getElementById("lar_"+resource+"_1").classList.add('do-transition');
                document.getElementById("lar_"+resource+"_2").classList.add('app-dis');
                document.getElementById("lar_"+resource+"_2").classList.add('do-transition');
                document.getElementById("lar_"+resource+"_3").classList.add('app-dis');
                document.getElementById("lar_"+resource+"_3").classList.add('do-transition');
            }

            rKeys = Object.keys(this.rewards.resources);

            rKeys.forEach((key, index) => {
                this.rewards.resources[key].position.left = this.getOffset( document.getElementById('tmr_' + key) ).left;
                this.rewards.resources[key].position.top = this.getOffset( document.getElementById('tmr_' + key) ).top;
            });
        },
        hideElement: function(elName, duration = 0.5, delay = 0) {
            document.getElementById(elName).style.animation = "disappear-opacity " + duration + "s " + delay + "s both";
        },
        //set lights for the time machine core
        stmLightsBackground: function() {
            nOfResources = 0;
            for(resource in this.inventory.resources) {
                if(this.inventory.resources[resource].active)
                    nOfResources += 1;
            }

            pieceSize = 100/nOfResources;

            background = "radial-gradient( circle closest-side, transparent 100%, transparent 100% ), conic-gradient(";

            i=1;

            for(resource in this.inventory.resources) {
                if(!this.inventory.resources[resource].active)
                    continue;

                if(i > 1)
                    background += ", ";

                if(this.inventory.resources[resource].fill >= 100)
                    color = this.inventory.resources[resource].color;
                else
                    color = "#FFF";
                percentage = pieceSize * i - 1;

                background += color + " 0%, " + color + " " + percentage + "%, #333 0%, #333 " + (percentage + 1) + "%";
                i++;
            }
            background += ")";

            document.getElementById("stm-lights").style.background = background;
        },

        //Check if reuirements for time machine upgrade are met
        upgradeReadyCheck: function() {
            ready = true;

            for(resource in this.inventory.resources) {
                //skip inactive resources
                if(!this.inventory.resources[resource].active)
                    continue;
                
                if(this.inventory.resources[resource].fill < 100)
                    ready = false;
            }

            return ready;
        },

        //Upgrade time machine
        timeMachineUpgrade: function() {
            //Check that time machine is really ready to be upgraded
            if(!this.upgradeReadyCheck())
                return;
            
            if(this.upgrading == true)
                return;
            
            this.upgrading = true;
            
            document.getElementById("stm").classList.add('upgrading');
            
            setTimeout(function () { 
                reward = this.progression[this.level].reward.type;
                
                if(reward == "special-path") {
                    this.startRun("specialPathUnlock");
                }
                else if(reward == "era") {
                    this.startRun("eraUnlock");
                }
                else {
                    this.inventory.keys += 1;
                    this.inventory.chests += 1;

                    this.changeScene("lootBoxOpening");
                }
                
                //Update amount of resources as some are spent for update
                for(resource in this.inventory.resources) {
                    //skip if resource is not used
                    if(!this.inventory.resources[resource].active)
                       continue;
                    this.inventory.resources[resource].amount -= this.progression[this.level].resources[resource];
                }

                //Level up
                this.level++;
                
                //higher requirement for loot boxes
                this.goldBarsRequirement = 40 + 20 * this.level;

                //Make sure that all the containers for this level show up
                for(resource in this.progression[this.level].resources) {
                    this.inventory.resources[resource].active = true;
                }

                //Update the container fill
                this.preFillRewards();
                this.fillRewards();

                //Update lights on the time machine
                this.stmLightsBackground();
            }.bind(this), 2500);
        },
        //Start the run sequence
        gameOverStartRun: function() {
            
            this.changeScene("home");
            this.startRun();
        },

        //Start the run sequence
        startRun: function(preset = false) {
            if(document.getElementById("sh-bottom-navigation"))
                document.getElementById("sh-bottom-navigation").style.display = "none"
            
            if(this.runnerVideos[this.getActiveVideo().toString()])
                this.runnerVideos[this.getActiveVideo().toString()].active = false;
            
            wait = 0;
            if(this.activeScene != "home") {
                this.changeScene("home");
                wait = 500;
            }

                
            setTimeout(function () {
                
                document.getElementById('home').classList.add('startRunning');
                
            }.bind(this), wait);
            
            if(!preset) {
                randomVideos = new Array();
                //Select a random video
                for(runnerVideo in this.runnerVideos) {
                    //Skip if video level requirement is too high
                    if(this.runnerVideos[runnerVideo].minLevel > this.level)
                        continue;

                    randomVideos.push(runnerVideo);
                }

                //Select random video
                randomVideo = 1 + Math.floor(Math.random() * (randomVideos.length));
            }
            
            else {
                randomVideo = preset;
            }
            
            
            this.runnerVideos[randomVideo].active = true;
            
            
            //setTimeout(function () { this.loadYTvideo(randomVideo, this.runnerVideos[randomVideo].url) }.bind(this), 2000);
            setTimeout(function () { this.changeScene("running"); }.bind(this), 1900);
            setTimeout(function () { this.loadYTvideo() }.bind(this), 2000);
            //setTimeout(function () { this.changeScene("gameOver") }.bind(this), this.runnerVideos[randomVideo].duration);
        },
        loadYTvideo: function() {
            // 2. This code loads the IFrame Player API code asynchronously.
            if(!this.ytApiReady) {
                
                var tag = document.createElement('script');

                tag.src = "https://www.youtube.com/iframe_api";
                var firstScriptTag = document.getElementsByTagName('script')[0];
                firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

                this.ytApiReady = true;
            }
            else
            onYouTubeIframeAPIReady();
        },
        getActiveVideo: function() {
            activeVideo = false;
            
            for(runnerVideo in this.runnerVideos) {
                //Skip if video level requirement is too high
                if(this.runnerVideos[runnerVideo].active)
                    activeVideo = runnerVideo;
            }

            return activeVideo;
        },
        getActiveVideoID: function() {
            return "video_" + this.getActiveVideo();
        },
        getActiveVideoUrl: function() {
            return this.runnerVideos[this.getActiveVideo().toString()].url;
        },
        getActiveVideoGoldBars: function() {
            return this.runnerVideos[this.getActiveVideo().toString()].goldbars;
        },
        getActiveVideoScore: function() {
            return this.runnerVideos[this.getActiveVideo().toString()].score;
        },
        //Game over screen
        gameOver: function() {
            this.upgrading = false;
            
            if(this.gameOvering)
                return;
            
            this.gameOvering = true;
            
            oLeft = this.getOffset( document.getElementById("c-gbi") ).left;
            oTop = this.getOffset( document.getElementById("c-gbi") ).top;

            for(i=1; i<=3; i++) {
                document.getElementById("gbr-" + i).style.left = oLeft + "px";
                document.getElementById("gbr-" + i).style.top = oTop + "px";
                document.getElementById("gbr-" + i).style.opacity = 1;
            }
            
            setTimeout(function () {
                oLeft = this.getOffset( document.getElementById("lbp-bar") ).left;
                oTop = this.getOffset( document.getElementById("lbp-bar") ).top;
                
                for(i=1; i<=3; i++) {
                    document.getElementById("gbr-" + i).style.left = oLeft + "px";
                    document.getElementById("gbr-" + i).style.top = oTop + "px";
                    document.getElementById("gbr-" + i).classList.add("make-it-disappear");
                }
            }.bind(this), 200);

            setTimeout(function () {
                //Update the gold bar quantity
                this.inventory.goldBars += this.getActiveVideoGoldBars();

                if(this.chestReadyCheck()) {
                    setTimeout(function () {

                        this.gameOverFill();
            
                    }.bind(this), 1300);
                }
                else {
                    this.gameOvering = false;
                }
            }.bind(this), 200);
        },
        gameOverFill: function() {
            //Starting point for the chest
            this.setFloatyChestInitialPosition();

            document.getElementById('lbp').classList.add('claim');
            document.getElementById('lbp-chest-floaty').classList.add('claim');
            document.getElementById('lb-ep').classList.add('show');
            

            setTimeout(function () {
                //End point for the chest
                oLeft = this.getOffset( document.getElementById("lb-ep") ).left;
                oTop = this.getOffset( document.getElementById("lb-ep") ).top;
    
                document.getElementById("lbp-chest-floaty").style.left = oLeft + "px";
                document.getElementById("lbp-chest-floaty").style.top = oTop + "px";
            }.bind(this), 100);

            //Add a chest to inventory
            this.inventory.chests += 1;

            this.inventory.goldBars -= this.goldBarsRequirement;

            this.resetGoldBar = true;
            
            setTimeout(function () {
                this.gameOverClaimReset();
            }.bind(this), 2500);
        },
        setFloatyChestInitialPosition: function() {
            oLeft = this.getOffset( document.getElementById("lbp-chest-main") ).left;
            oTop = this.getOffset( document.getElementById("lbp-chest-main") ).top;

            document.getElementById("lbp-chest-floaty").style.left = oLeft + "px";
            document.getElementById("lbp-chest-floaty").style.top = oTop + "px";
        },
        gameOverClaimReset: function() {
            document.getElementById('lbp').classList.remove('claim');
            document.getElementById('lbp-chest-floaty').classList.remove('claim');
            this.resetGoldBar = false;
            
            this.setFloatyChestInitialPosition();

            if(this.chestReadyCheck()) {
                setTimeout(function () {
                    this.gameOverFill();
                }.bind(this), 2000);
            }
            else {
                this.gameOvering = false;
            }
        },
        //player has enough gold bars to claim another chest
        chestReadyCheck: function() {
            return this.inventory.goldBars >= this.goldBarsRequirement;
        },
        keyGeneration: function() {
            //player has no keys
            if(!this.inventory.keys) {
                this.nextKey -= 1;

                //next key is ready
                if(this.nextKey <= 0) {
                    this.inventory.keys += 1;
                    this.nextKey = 120;
                }
            }

            setTimeout(function () {
                this.keyGeneration();
            }.bind(this), 1000);
        },
        chestCanOpen: function() {
            return this.inventory.chests > 0 && this.inventory.keys > 0;
        },
    },

    computed: {
        //Calculate progress towards the new loot box
        goldBarFill: function() {
            perc = this.inventory.goldBars/this.goldBarsRequirement * 100;
            progress = 0;

            if(this.resetGoldBar)
                return 0;

            if(perc >= 100)
                progress = 100;
            else if (perc == 0)
                progress = 0;
            else if (perc >= 90)
                progress = 90;
            else
                progress = perc;
                //progress = 10 + this.goldBarsRequirement/100*this.inventory.goldBars*0.79;

            return progress;
        },
        showChestGlow: function() {
            return (!this.activeScene.lootBoxOpening || this.lootBoxOpening != 1)
        },
        upgradeReady: function() {
            return this.upgradeReadyCheck();
        },
        chestReady: function() {
            return this.chestReadyCheck;
        },
        keyCountdown: function() {
            return this.nextKey + "s";
        },
        returnActiveTimeMachineImage: function() {
            return this.progression[this.level].reward.image;
        },
        chestOpen: function() {
            return this.chestCanOpen();
        },
    },
    mounted(){
        this.keyGeneration();
    },
})

  function onYouTubeIframeAPIReady() {

    // 3. This function creates an <iframe> (and YouTube player)
    //    after the API code downloads.
    var player;
    player = new YT.Player(app.getActiveVideoID(), {
      width: '100%',
      videoId: app.getActiveVideoUrl(),
      playerVars: { 'autoplay': 1, 'playsinline': 1 },
      events: {
        'onReady': onPlayerReady,
        'onStateChange': onPlayerStateChange,
      }
    });
  }

  

  // 4. The API will call this function when the video player is ready.
  function onPlayerReady(event) {
    event.target.playVideo();
  }

  // 5. The API calls this function when the player's state changes.
  //    The function indicates that when playing a video (state=1),
  //    the player should play for six seconds and then stop.
  var done = false;
  function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.PLAYING && !done) {
      setTimeout(stopVideo, 6000);
      done = true;
    }
  }
  function stopVideo() {
    player.stopVideo();
  }
  
// when video ends
function onPlayerStateChange(event) {        
    if(event.data === 0) {          
        app.changeScene('gameOver')
    }
}*/