var app = new Vue({
    el: "#app",

    data: {
        goldBarsRequirement: 100,
        nextKey: 120,
        level: 1,
        csSelected: "tom",
        lboStep: "chest",
        charSelectorOpen: false,
        stickerProcessing: false,
        stickerProcessingCounter: 0,
        activeScene: {
            home: true,
            lootBoxes: false,
            lootBoxOpening: false,
        },
        inventory: {
            hardCurrency: 0,
            chests: 2,
            goldBars: 0,
            keys: 2,
        },
        duplicates: {
            /*1: {
                image: 'images/stickers/tom_2_c_1.jpg',
                gold: 5,
            },
            2: {
                image: 'images/stickers/tom_2_c_2.jpg',
                gold: 10,
            },
            3: {
                image: 'images/stickers/tom_2_c_5.jpg',
                gold: 20,
            }*/
        },
        characters: {
            tom: {
                mainScene: 1,
                selectedOutfit: 1,
                tryOutfit: 1,
                outfits: {
                    1: {
                        type: 'unlocked',
                    },
                    2: {
                        type: 'stickers',
                        stickers: {
                            1: false,
                            2: false,
                        }
                    },
                    3: {
                        type: 'stickers',
                        stickers: {
                            1: false,
                            2: false,
                            3: false,
                            4: false,
                            5: false,
                            6: false,
                            7: false,
                            8: false,
                            9: false,
                            10: false,
                            11: false,
                            12: false,
                        },
                    },
                },
            },
            angela: {
                mainScene: 2,
                selectedOutfit: 1,
                tryOutfit: 1,
                outfits: {
                    1: {
                        type: 'unlocked',
                    },
                    2: {
                        type: 'stickers',
                        stickers: {
                            1: false,
                            2: false,
                            3: false,
                            4: false,
                            5: false,
                            6: false,
                            7: false,
                            8: false,
                            9: false,
                            10: false,
                            11: false,
                            12: false,
                        }
                    },
                    3: {
                        type: 'purchasable',
                    },
                },
            },
            hank: {
                mainScene: 3,
                selectedOutfit: 1,
                tryOutfit: 1,
                outfits: {
                    1: {
                        type: 'unlocked',
                    },
                    2: {
                        type: 'stickers',
                        stickers: {
                            1: false,
                            2: false,
                            3: false,
                            4: false,
                            5: false,
                            6: false,
                            7: false,
                            8: false,
                            9: false,
                            10: false,
                            11: false,
                            12: false,
                        }
                    },
                    3: {
                        type: 'purchasable',
                    },
                },
            }
        },
        lootBoxOpeningStickers: {
            lootBoxOpening: 1,
            stickerSeq: 0,
            stickerSeqCounter: 0,
            character: false,
            outfit: false,
            sticker: false,
            stickerCounter: false,
            stickerStatus: "back",
            stickerReady: false,
            lootTable: {
                1: {
                    1: {
                        character: 'tom',
                        outfit: 2,
                        sticker: 1,
                    },
                    2: {
                        character: 'tom',
                        outfit: 2,
                        sticker: 2,
                    },
                    3: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 8,
                    },
                },
                2: {
                    1: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 3,
                    },
                    2: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 2,
                    },
                    3: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 9,
                    },
                },
                3: {
                    1: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 7,
                    },
                    2: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 9,
                    },
                    3: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 1,
                    },
                    4: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 4,
                    },
                    5: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 6,
                    },
                    6: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 11,
                    },
                    7: {
                        character: 'hank',
                        outfit: 2,
                        sticker: 1,
                    },
                    8: {
                        character: 'hank',
                        outfit: 2,
                        sticker: 7,
                    },
                    9: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 9,
                    },
                    10: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 8,
                    },
                },
                4: {
                    1: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 6,
                    },
                    2: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 10,
                    },
                    3: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 3,
                    },
                    4: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 5,
                    },
                    5: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 7,
                    },
                    6: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 10,
                    },
                    7: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 12,
                    },
                    8: {
                        character: 'hank',
                        outfit: 2,
                        sticker: 5,
                    },
                    9: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 3,
                    },
                    10: {
                        character: 'angela',
                        outfit: 2,
                        sticker: 4,
                    },
                },
                5: {
                    1: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 6,
                    },
                    2: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 6,
                    },
                    3: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 6,
                    },
                    4: {
                        character: 'tom',
                        outfit: 3,
                        sticker: 6,
                    },
                },
            },
        },
    },

    methods: {
        //Transition from one scene to another
        changeScene: function(newScene) {
            //Hide all scenes
            for (scene in this.activeScene) {
                this.activeScene[scene] = false;
            }

            //Show new scene
            this.activeScene[newScene] = true;

            //Reset loot box opening
            this.lootBoxOpening = 1;
            
            if(newScene != "characterSelection")
                this.csSelected = false;
            
            else
                this.unlockNewOutfits();
            
            this.charSelectorOpen = false;
        },

        //Open loot box sequence
        openLootBox: function(type = "chest") {
            this.changeScene('lootBoxOpening')

            if(type == "chest") {
                this.inventory.chests -= 1;
                this.inventory.keys -= 1;
            }
            else {
                this.inventory.hardCurrency -= 300;
            }

            this.lboStep = type;
            this.lboNextStep();
        },

        //Generate rewards
        generateRewards: function() {
        },

        //get absolute position of element relative to window
        getOffset: function( el ) {
            var _x = 0;
            var _y = 0;
            while( el && !isNaN( el.offsetLeft ) && !isNaN( el.offsetTop ) ) {
                _x += el.offsetLeft - el.scrollLeft;
                _y += el.offsetTop - el.scrollTop;
                el = el.offsetParent;
            }
            return { top: _y, left: _x };
        },

        setNewPosition: function(el) {
            return false;
        },

        hideElement: function(elName, duration = 0.5, delay = 0) {
            document.getElementById(elName).style.animation = "disappear-opacity " + duration + "s " + delay + "s both";
        },

        scaleUpElement: function(elName, duration = 0.5, delay = 0) {
            document.getElementById(elName).style.animation = "scale-up-center " + duration + "s " + delay + "s both";
        },

        scaleDownElement: function(elName, duration = 0.5, delay = 0) {
            document.getElementById(elName).style.animation = "scale-up-center " + duration + "s " + delay + "s both";
        },
        
        setFloatyChestInitialPosition: function() {
            oLeft = this.getOffset( document.getElementById("lbp-chest-main") ).left;
            oTop = this.getOffset( document.getElementById("lbp-chest-main") ).top;

            document.getElementById("lbp-chest-floaty").style.left = oLeft + "px";
            document.getElementById("lbp-chest-floaty").style.top = oTop + "px";
        },
        
        keyGeneration: function() {
            //player has no keys
            if(!this.inventory.keys) {
                this.nextKey -= 1;

                //next key is ready
                if(this.nextKey <= 0) {
                    this.inventory.keys += 1;
                    this.nextKey = 120;
                }
            }

            setTimeout(function () {
                this.keyGeneration();
            }.bind(this), 1000);
        },
        getCharacterOutfitImage: function(character, outfitID) {
            return 'images/' + character + '_' + outfitID + '.png';
        },
        getCharacterSelectedOutfit: function(character) {
            return this.characters[character].selectedOutfit;
        },
        getCharacterTryOutfit: function(character) {
            return this.characters[character].tryOutfit;
        },
        goToCS: function(character) {
            this.csSelected = character;
            this.changeScene("characterSelection");
        },
        changeOutfit: function(outfitID) {
            this.characters[this.csSelected].selectedOutfit = outfitID;
        },
        //selects a new outfit in character selection scene
        changeTryOutfit: function(outfitID) {
            this.characters[this.csSelected].tryOutfit = outfitID;
        },
        //saves the outfits for all characters, ignores if outfit is locked
        confirmOutfitChange: function() {
            for(character in this.characters) {
                if(this.characters[character].outfits[this.characters[character].tryOutfit].type == "unlocked" || this.characters[character].outfits[this.characters[character].tryOutfit].type == "new")
                    this.characters[character].selectedOutfit = this.characters[character].tryOutfit;
                else
                    this.characters[character].tryOutfit = this.characters[character].selectedOutfit;
            }
            this.changeScene("home");
        },
        //saves the newly selected character
        confirmCharChange: function() {
            count = 2;
            for(character in this.characters) {
                if(character == this.csSelected) {
                    this.characters[character].mainScene = 1;
                }
                else {
                    this.characters[character].mainScene = count;
                    count++;
                }
            }
            this.confirmOutfitChange();
        },
        //returns an image of a sticker (unlocked or not)
        getSticker: function(character, outfit, sequence) {
            if(this.characters[character].outfits[outfit].stickers[sequence]) {
                return "images/stickers/" + character + "_" + outfit + "_c_" + this.zeroPad(sequence,2) + ".jpg"
            }
            else {
                return "images/stickers/" + character + "_" + outfit + "_" + this.zeroPad(sequence,2) + ".jpg"
            }
        },
        //returns an image of an active sticker
        getActiveSticker: function() {
            lootBoxOpeningTable = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][this.lootBoxOpeningStickers.stickerSeq];
            character = lootBoxOpeningTable.character;
            outfitID = lootBoxOpeningTable.outfit;
            sticker = lootBoxOpeningTable.sticker;

            return "images/stickers/" + character + "_" + outfitID + "_c_" + this.zeroPad(sticker,2) + ".jpg"
        },
        //return the number of stickers still remining in the loot box opening
        lboStickersRemaining: function() {
            return Object.keys(this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening]).length - this.lootBoxOpeningStickers.stickerSeqCounter;
        },
        //one function to take care of sequencing the loot box opening
        lboNextStep: function() {
            if(this.lboStep == "chest" || this.lboStep == "premium") {
                if(this.lootBoxOpeningStickers.lootBoxOpening == 2) {
                    this.inventory.hardCurrency = 600;
                }

                timer = this.lboStep == "chest" ? 2300 : 1200;
                setTimeout(function () { this.lboStep ="stickersRewardPreview"; }.bind(this), timer);
            }
            else if(this.lboStep == "stickersRewardPreview") {
                this.lootBoxOpeningStickers.stickerCounter = true;
                this.lboStep = "stickerUnveiling";
                setTimeout(function () { this.lboNextStep() }.bind(this), 500);
            }
            else if(this.lboStep == "stickerUnveiling") {
                //last sticker & no duplicates
                if(this.lootBoxOpeningStickers.stickerSeq >= Object.keys(this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening]).length) {
                    this.lboStep = "end";
                    this.lboNextStep();
                    this.lootBoxOpeningStickers.stickerCounter = false;
                    return false;
                }

                //Increase the sticker that is being unveiled
                this.lootBoxOpeningStickers.stickerSeq++;

                lootBoxOpeningTable = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][this.lootBoxOpeningStickers.stickerSeq];
                character = lootBoxOpeningTable.character;
                outfitID = lootBoxOpeningTable.outfit;
                sticker = lootBoxOpeningTable.sticker;

                if(this.characters[character].outfits[outfitID].stickers[sticker]) {
                    this.lboStep = "duplicates";
                    this.lboNextStep();
                    return false;
                }

                this.csSelected = character;
                this.characters[character].tryOutfit = outfitID;

                this.lootBoxOpeningStickers.stickerReady = true;

                lootBoxOpeningTable = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][this.lootBoxOpeningStickers.stickerSeq];
                character = lootBoxOpeningTable.character;
                outfitID = lootBoxOpeningTable.outfit;
                sticker = lootBoxOpeningTable.sticker;
                
                //sticker back starting position
                setTimeout(function () {
                    stickerPileLeft = this.getOffset(document.getElementById("sticker-pile")).left;
                    stickerPileTop = this.getOffset(document.getElementById("sticker-pile")).top;
                    stickerWidth = document.getElementById("sticker-back").offsetWidth;
                    stickerHeight = document.getElementById("sticker-back").offsetHeight;


                    document.getElementById("sticker-back").style.left = (stickerPileLeft - stickerWidth/2) + "px";
                    document.getElementById("sticker-back").style.top = (stickerPileTop - stickerHeight/2) + "px";
                }.bind(this), 300);


                //sticker back shows up and settles under the pile
                setTimeout(function () {
                    //reduce the counter for 1
                    this.lootBoxOpeningStickers.stickerSeqCounter = this.lootBoxOpeningStickers.stickerSeq;

                    //sticker back grow in size
                    document.getElementById("sticker-back").style.transform = "scale(1)";
                    //sticker back show
                    document.getElementById("sticker-back").style.opacity = "1";
                    
                    stickerPileTop = this.getOffset(document.getElementById("sticker-pile")).top;
                    stickerHeight = document.getElementById("sticker-back").offsetHeight;

                    document.getElementById("sticker-back").style.top = (stickerPileTop + 30) + "px";
                }.bind(this), 600);


                //sticker back move to center (front does the same but is hidden)
                setTimeout(function () {
                    screenWidth = screen.width;
                    screenHeight = screen.height;

                    stickerWidth = document.getElementById("sticker-back").offsetWidth;
                    stickerHeight = document.getElementById("sticker-back").offsetHeight;

                    offsetLeft = screenWidth / 2 - stickerWidth / 2;
                    offsetTop = screenHeight / 2 - stickerHeight / 2;

                    maxScale = this.isTutorialOutfit ? "scale(1)" : "scale(3)";
                    //set position to center
                    document.getElementById("sticker-back").style.left = offsetLeft + "px";
                    document.getElementById("sticker-back").style.top = offsetTop + "px";
                    //sticker back grow in size
                    document.getElementById("sticker-back").style.transform = maxScale;

                    //set position to center
                    document.getElementById("sticker-front").style.left = offsetLeft + "px";
                    document.getElementById("sticker-front").style.top = offsetTop + "px";
                    //sticker front scale to match large back
                    document.getElementById("sticker-front").style.transform = maxScale;
                }.bind(this), 900);

                //sticker back replaced with front (hide/show)
                setTimeout(function () {
                    //sticker back disappear
                    document.getElementById("sticker-back").style.opacity = 0;
                    //sticker front grow in size
                    document.getElementById("sticker-front").style.opacity = 1;
                }.bind(this), 1500);

                //sticker front moves to the final position
                setTimeout(function () {
                    stickerID = character + "_" + outfitID + "_" + sticker;

                    //sticker front move to position
                    document.getElementById("sticker-front").style.left = (this.getOffset(document.getElementById(stickerID)).left + 6) + "px";
                    document.getElementById("sticker-front").style.top = (this.getOffset(document.getElementById(stickerID)).top + 6) + "px";

                    //sticker front decrease in size
                    document.getElementById("sticker-front").style.transform = "scale(1)";
                }.bind(this), 2000);

                //reset sticker back and front
                setTimeout(function () {
                    stickerID = character + "_" + outfitID + "_" + sticker;

                    this.characters[character].outfits[outfitID].stickers[sticker] = true;

                    //sticker front hide
                    document.getElementById("sticker-front").style.opacity = "0";
                    //sticker front shrink
                    document.getElementById("sticker-front").style.transform = "scale(0)";
                    //sticker back shrink
                    document.getElementById("sticker-back").style.transform = "scale(0)";
                }.bind(this), 2500);

                setTimeout(function () {
                    if(this.albumFull(lootBoxOpeningTable.character, lootBoxOpeningTable.outfit)) {
                        setTimeout(function () {
                            this.lootBoxOpeningStickers.stickerCounter = false;
                            this.lboStep = "newOutfit";
                            this.lboNextStep();
                        }.bind(this), 1000);
                            
                    }
                    else {
                    this.lboNextStep();
                        
                    }
                }.bind(this), 3000);
            }
            else if(this.lboStep == "duplicates") {
                cn = 1;
                let sposition = this.lootBoxOpeningStickers.stickerSeqCounter;

                for(let i = this.lootBoxOpeningStickers.stickerSeqCounter; i < Object.keys(this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening]).length; i ++) {
                    Vue.set(this.duplicates, i, {});

                    character = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][i].character;
                    outfit = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][i].outfit;
                    sticker = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][i].sticker;
                    Vue.set(this.duplicates[i], "image", "images/stickers/" + character + "_" + outfit + "_c_" + this.zeroPad(sticker,2) + ".jpg");

                    rarity = this.getRarity();
                    coins = rarity == "normal" ? 2 : rarity == "rare" ? 5 : rarity == "epic" ? 10 : 20;
                    Vue.set(this.duplicates[i], "coins", coins);
                    
                    this.inventory.goldBars += coins;

                    this.moveDuplicateOne(i, cn);

                    cn++;
                }
                this.replaceDuplicatesWithGold(sposition, cn);

                let timeOffset = cn * 2000 + 500;
                
                setTimeout(function () {
                    this.lboStep = "end";
                    this.lboNextStep();
                }.bind(this), timeOffset);
            }
            else if(this.lboStep == "end") {
                this.lootBoxOpeningStickers.stickerCounter = false;
                this.lootBoxOpeningStickers.stickerSeq = 0;
                this.lootBoxOpeningStickers.stickerSeqCounter = 0;
                this.lootBoxOpeningStickers.lootBoxOpening += 1;

                for(character in this.characters) {
                    this.characters[character].tryOutfit = this.characters[character].selectedOutfit;
                }
                
                this.stickerProcessing = false;
                this.stickerProcessingCounter = 0;
                
                this.changeScene("lootBoxes");
            }
        },
        
        startStickerUnveiling: function() {
            if(this.stickerProcessingCounter > 0)
                return false;
            
            this.stickerProcessingCounter += 1;
            this.stickerProcessing = true;
            this.lboNextStep();
        },

        moveDuplicateOne: function(id, timerOffset) {
            let stickerID = "sdi-" + id;
            let fakeID = "sdf-" + id;
            let tOff = timerOffset;

            setTimeout(function () {
                console.log("inside: "+ stickerID);
                document.getElementById(stickerID).style.transform = "scale(1)";
    
                stickerPileLeft = this.getOffset(document.getElementById("sticker-pile")).left;
                stickerPileTop = this.getOffset(document.getElementById("sticker-pile")).top;
                stickerWidth = document.getElementById(stickerID).offsetWidth;
                stickerHeight = document.getElementById(stickerID).offsetHeight;
    
    
                document.getElementById(stickerID).style.left = (stickerPileLeft - stickerWidth/2) + "px";
                document.getElementById(stickerID).style.top = (stickerPileTop - stickerHeight/2) + "px";

                this.lootBoxOpeningStickers.stickerSeqCounter += 1;
                
            }.bind(this), 300 * tOff);
            setTimeout(function () {
                fakeLeft = this.getOffset(document.getElementById(fakeID)).left;
                fakeTop = this.getOffset(document.getElementById(fakeID)).top;
                
                document.getElementById(stickerID).style.left = fakeLeft + "px";
                document.getElementById(stickerID).style.top = fakeTop + "px";
            }.bind(this), 400 * tOff);
        },

        replaceDuplicatesWithGold: function(sposition, cn) {
            console.log(cn);
            let pos = sposition;
            let tOff = cn;
            for(let i = pos; i < Object.keys(this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening]).length; i ++) {
                setTimeout(function () {
                    let stickerID = "sdi-" + i;
                    let goldID = "gb-" + i;
                    console.log(stickerID + " & " + goldID)
                    document.getElementById(stickerID).style.transform = "scale(0)";
                    document.getElementById(goldID).style.transform = "scale(1)";
                }.bind(this), tOff * 700 + 400 * i);
            }
        },

        lboClaimOutfit: function() {
            //console.log(this.characters[csSelected].outfits[lootBoxOpeningTable.outfit].type)
            this.characters[this.csSelected].outfits[this.characters[this.csSelected].tryOutfit].type = "new";

            this.lboStep = "stickerUnveiling";
            this.lootBoxOpeningStickers.stickerCounter = true;
            this.lboNextStep();
        },
        
        //helper
        zeroPad: function(num, places) {
            var zero = places - num.toString().length + 1;
            return Array(+(zero > 0 && zero)).join("0") + num;
        },

        getRarity: function(stickerNumber) {
            stickerNumber = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][this.lootBoxOpeningStickers.stickerSeq].sticker;

            if(this.isTutorialOutfit) {
                return 'tutorial';
            }
            else if(Math.floor((stickerNumber - 1) / 3) == 0) {
                return 'normal';
            }
            else if(Math.floor((stickerNumber - 1) / 3) == 1) {
                return 'rare';
            }
            else if(Math.floor((stickerNumber - 1) / 3) == 2) {
                return 'epic';
            }
            else if(Math.floor((stickerNumber - 1) / 3) == 3) {
                return 'legendary';
            }
        },

        getStickerBack: function() {
            stickerNumber = this.lootBoxOpeningStickers.lootTable[this.lootBoxOpeningStickers.lootBoxOpening][this.lootBoxOpeningStickers.stickerSeq].sticker;
            
            return 'images/sticker-back-' + this.getRarity(stickerNumber) +'.png';
        },

        albumFull: function(character, outfit) {
            albumFull = true;

            for(sticker in this.characters[character].outfits[outfit].stickers) {
                if(!this.characters[character].outfits[outfit].stickers[sticker]) {
                    albumFull = false;
                    break;
                }
            }

            return albumFull;
        },

        changeCharacterInFocus: function(character) {
            this.csSelected = character;
            this.charSelectorOpen = false;
            this.unlockNewOutfits();
        },
        
        unlockNewOutfits: function() {
            for(ou in this.characters[this.csSelected].outfits) {
                if(this.characters[this.csSelected].outfits[ou].type == "new") {
                    let outf = ou;
                    setTimeout(function () {
                        console.log(this.characters[this.csSelected].outfits[outf].type)
                        this.characters[this.csSelected].outfits[outf].type = "unlocked";
                    }.bind(this), 1000);
                }
            }
            console.log(this.characters)
        }
    },

    computed: {
        //Calculate progress towards the new loot box
        goldBarFill: function() {
            perc = this.goldBarsRequirement/100*this.inventory.goldBars;
            progress = 0;

            if(this.resetGoldBar)
                return 0;

            if(perc >= 100)
                progress = 100;
            else if (perc == 0)
                progress = 0;
            else
                progress = 10 + this.goldBarsRequirement/100*this.inventory.goldBars*0.79;

            return progress;
        },
        showChestGlow: function() {
            return (!this.activeScene.lootBoxOpening || this.lootBoxOpening != 1)
        },
        upgradeReady: function() {
            return this.upgradeReadyCheck();
        },
        chestReady: function() {
            return this.chestReadyCheck;
        },
        keyCountdown: function() {
            return this.nextKey + "s";
        },
        getOutfitType: function() {
            return this.characters[this.csSelected].outfits[this.characters[this.csSelected].tryOutfit].type;
        },
        charTryOutfitID: function() {
            return this.characters[this.csSelected].tryOutfit;
        },
        selectedCharOutfitImg: function() {
            return this.getCharacterOutfitImage(this.csSelected, this.getCharacterTryOutfit(this.csSelected));
        },
        selectedOutfitAlbumPage: function() {
            return this.characters[this.csSelected].outfits[this.characters[this.csSelected].tryOutfit].stickers;
        },
        isTutorialOutfit: function() {
            if(this.csSelected == "tom" && this.characters.tom.tryOutfit == 2)
                return true;

            return false;
        },
        showStickerCounter: function() {
            return this.lootBoxOpeningStickers.stickerCounter;
        }
    },
    mounted(){
        this.keyGeneration();
    },
})